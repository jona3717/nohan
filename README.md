# NoHAN

Nohan es un asistente que ayuda al usuario con algunas tareas como, abrir programas, realizar búsquedas en internet, etc. Esta aplicación es desarrollada para trabajar desde el Krunner del entorno de escritorio Plasma o también desde una terminal, además cuenta con reconocimiento de voz.


### Pre-requisitos 📋

Nohan ha sido desarrollado para trabajar con el Krunner de Plasma, en las distribuciones basadas en Archlinux y Debian, sin embargo, puede funcionar en cualquier otro entorno desde una terminal o en cualquier distribución, con algunas limitaciones.
Las dependencias se encuentran en los archivos: dependencias (Asgúrate de tener instaladas estas dependencias) y requirements.txt (estas se instalarán al ejecutar el archivo "instalar" desde una terminal).

### Instalación 🔧

Para realizar la instalación de NoHAN se debe ejecutar el archivo instalar desde la terminal. Se deberá ingresar la contraseña para poder realizar la instalación de las dependencias, si la instalación se realiza en una distrbución no basada en Archlinux o Debian, se visualizará un mensaje de advertencia y pedirá un mensaje de confirmación para proceder o cancelar la instalación. NoHan, cuenta con la opción para utilizarse mediante comandoz de voz. Si quieres activar esta opción deberás configurar el reconocimiento de voz. Esta característica ha sido desarrollada utilizando SpeechRecognition, con la librería de google. Puedes optar por no configurarlo para trabajar únicamente con comandos de texto.

```
./instalar
```

## Ejecutando las pruebas ⚙️

Para usar NoHAN basta con iniciar el Krunner con el atajo de teclado predeterminado ```(ALT + ESPACIO)```, tu atajo personalizado o una terminal y llamarlo por su nombre e indicar la acción a realizar. Para visualizar todos los comandos de NoHAN, incluidos los de voz, puedes ver la ayuda con el comando:

```
nohan ayuda
```

## Construido con 🛠️

* [Python](https://www.python.org/) - El lenguaje utilizado para el desarrollo
* [PyQt5](https://pypi.org/project/PyQt5/) - La librería utilizada para el icono de notificación de NoHAN
* [Vim](https://www.vim.org/) - El editor utilizado para el desarrollo del proyecto.

## Autores ✒️

* **Jonathan Córdova** - *Desarrollo y documentación* - [jona3717](https://gitlab.com/jona3717)

## Licencia 📄

Este proyecto está bajo la Licencia (GPLv3) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 al desarrollador. 
* Da las gracias públicamente 🤓.
* etc.



---
