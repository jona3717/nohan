#!/usr/bin/env python
__author__ = "jona3717"

import os
import os.path as path
import subprocess
import speech_recognition as sr


def copy_files():
    """Copia los archivos de la aplicación en las carpetas respectivas,
        instala las librerías necesarias y crea los directorios en los
        que se guardarán los archivos del usuario"""

    os.system('sudo cp -r nohan /usr/share/')
    os.system('sudo cp nohan/nohan /usr/bin/')
    os.system('sudo pip install -r requirements.txt')
    os.system('mkdir -p ~/.nohan/data && mkdir -p ~/.nohan/audio')


def user_recognition():
    output = subprocess.check_output("whoami", shell=True).splitlines()
    phrase = str(output[0])
    user = phrase[2:len(phrase) - 1]
    return user


def distro_identify():
    """ Identifica si la distribución en la que se está instalando
        NoHan, está basada en Arch o Debian e instala las dependencias, de otro modo le informa al
         usuario, que podría no funcionar correctamente con algunas funciones"""
    if path.exists('/etc/arch-release'):
        os.system('sudo pacman -S libnotify python-gobject mpg123')
    elif path.exists('/etc/debian_version'):
        os.system('sudo apt install libnotify-bin')
    else:
        answer = ""
        answer = input(str('\nNoHAN puede presentar errores o no ejecutar correctamente\n'
                           'algunas funciones en tu distribución, ¿deseas instalar de todas formas?[s/n]:'))
        if len(answer) == 0 or answer != 's' or answer != 'S':
            os.system('chmod +x desinstalar')
            os.system('./desinstalar')
            print('\nSe eliminaron todo los archivos de NoHAN de tu sistema. Finalizado.')


def terminal_selection():
    options = {'1' : 'konsole', '2' : 'terminator', '3' : 'alacritty'}
    """ Pide al usuario que seleccione la terminal a utilizar."""
    print('')
    print('Selecciona una opción a contiunación:\n')
    print('1. Konsole')
    print('2. Terminator')
    print('3. Alacritty')
    print('4. Otro\n')
    print('')
    selection = input('> ')
    if selection.isnumeric() and (int(selection) > 0 and int(selection) < 4):
        user = user_recognition()
        f = open(f'/home/{user}/.nohan/data/term-emulator', 'w')
        f.write(options.get(selection))
        f.close()
    elif selection.isnumeric and selection == '4':
        terminal = input(str('Ingresa el nombre del binario de tu terminal'
            '(puedes encontrarlo en /usr/bin/): '))
        print('Para asegurar que el nombre es correcto, se ejecutará '
                'la terminal, deberás cerrarla y responder a continuación.')
        os.system(terminal)
        answer = input(str('¿Se ejecutó la terminal? [s/n]: '))
        if answer.lower() == 's':
            user = user_recognition()
            f = open(f'/home/{user}/.nohan/data/term-emulator', 'w')
            f.write()
            f.close()
    else:
        print('Opción inválida.')
        terminal_selection()


def capture_transcrib_speech(recognizer, microphone):
        """Transcribe lo que se graba desde `microphone`.

        Devuelve un diccionario con tres claves:
        "success":  un booleano indicando si la solicitud de la API
                    fue exitosa.
        "error":   `None` si no ocurre ingún error, de otro modo una cadena que contenga
                    un mensaje de error si no se pudo conectar con la API o no
                    se reconoció lo dicho.
        "transcription": `None` si no se transcribió la voz,
                    de otro modo una cadena que contenga el texto transrito
        """
        # verifica que los argumentos recognizer y microphone sean del tipo correcto
        if not isinstance(recognizer, sr.Recognizer):
            raise TypeError("`recognizer` must be `Recognizer` instance")

        if not isinstance(microphone, sr.Microphone):
            raise TypeError("`microphone` must be `Microphone` instance")

        # ajusta la sensibilidad de reconocimiento al ruido de ambiente y graba
        # el audio del micrófono
        with microphone as source:
            recognizer.adjust_for_ambient_noise(source)
            audio = recognizer.listen(source)

        # creamoos ej objeto response
        response = {
            "success": True,
            "error": None,
            "transcription": None
        }

        # hacemos un try del reconocimiento de vos en la grabación
        # si hay una exepción RequestError o UnknownValueError, se
        # captura para actualizar el objeto response respectivamente
        try:
            response["transcription"] = recognizer.recognize_google(audio, language="es-ES").lower()
        except sr.RequestError:
            # no se puede conectar a la API o no responde
            response["success"] = False
            response["error"] = "API unavailable"
        except sr.UnknownValueError:
            # no se entiende lo que se dijo
            response["error"] = "Unable to recognize speech"

        return response


def voice():
    """Configura el reconocimiento de voz del usuario. realiza tres
        grabaciones, las transcribe y las almacena en el archivo de
        comandos del usuario"""
    comandos = []
    recognizer = sr.Recognizer()
    microphone = sr.Microphone()
    print("\nPara que nohan pueda reconocer tu voz debes repetir\n tres frases que se mostrarán a continuación.\n")
    count = 1
    count2 = 0
    phrases = ["Hola nohan, ayuda", "Hola nohan, acualiza", "Hola nohan, busca"]
    while count < 4:
        print("\n{}. {}".format(count, phrases[count2]))
        speech = capture_transcrib_speech(recognizer, microphone)
        if len(speech["transcription"].split()) == 3:
            words = " ".join(speech["transcription"].split()[:2])
            if words:
                comandos.append(words)
            print("Reconocido.\n")
            count += 1
            count2 += 1
        else:
            print("Lo siento, hablaste muy rápido y no pude ententer. Repitamos.")

    user = user_recognition()
    os.system("touch ~/.nohan/data/comandos-usuario-voz")
    file = open("/home/{}/.nohan/data/comandos-usuario-voz".format(user), 'w')
    file.write('\n'.join(comandos))
    file.close()
    print("\nReconocimiento de voz terminado.")


def install():
    """Ejecuta las funciones necesarios para la instalación
        de NoHan. Permite al usuario configurar o no, la
        asistencia por voz."""
    distro_identify()
    copy_files()
    os.system('clear')
    terminal_selection()
    answer = input(str("¿Deseas instalar la asistencia por voz? (s/n): "))
    if answer == 'n' or answer == 'N':
        # elimina el módulo de asistencia por voz
        if path.isfile('/usr/share/nohan/source/voz.py'):
            os.system('sudo rm /usr/share/nohan/source/voz.py')
    elif answer == "s" or answer == "S":
        # ejecuta la función para configurar el reconocimiento de voz
        os.system('clear')
        voice()
    else:
        print("\nLa opción seleccionada no es válida")
        install()
    # verifica que el directorio principal de NoHan exista en el sistema
    if path.exists('/usr/share/nohan/'):
        os.system('clear')
        print('\nLa instalación ha finalizado con éxito.')
    else:
        print('\nLo siento, parece que ha ocurrido un error durante la instalación.')
        print('Por favor vuelve a intentarlo o counícate con el desarrollador.')


# ejecuta la función install
install()
