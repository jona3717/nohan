#!/usr/bin/env/ python
__author__ = "Jona3717"

"""
Este es NoHaN, un "asistente" de escritorio diseñado por un usuario de Plasma para usuarios de plasma,
pero no quiere decir que no pueda ser usado desde cualquier otro entorno de escritorio, usando una terminal.
"""
import sys
from source.nohan import Nohan

if __name__ == '__main__':
    Nohan()
    sys.exit()
