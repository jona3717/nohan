__author__ = 'Jona3717'

import subprocess
import sys
import os
import os.path as path
from source.menu import Menu

class LibraryList:
    # This class content the add, list and remove libraries methods
    def __init__(self):
        self.library_path = ''
        self.libraries = []

        self.user = self.user_id()
        self.menu()

    def menu(self):
        options = {
                '1':self.add_library,
                '2':self.delete,
                '3':self.list_libraries,
                '0':sys.exit
                }
        list_options = ('Agregar', 'Quitar', 'Listar', 'Cancelar')
        d_menu = Menu()
        option = d_menu.menu_options(list_options, 'Menú de Bibliotecas',
                                     'Selecciona una opción')
        if len(option) > 0 and option.isnumeric():
            os.system('clear')
            options.get(option, sys.exit)()
            self.menu()

    def user_id(self):
        # Identify the user name and return this
        out = subprocess.check_output('whoami', shell=True).splitlines()
        element = str(out[0])
        user = element[2:len(element)-1]
        return user

    def list_libraries(self):
        # Open the file that contains the names of libraries to
        # add dis in the list
        if path.exists(f'/home/{self.user}/.nohan/data/libraries_list'):
            my_file = open(f'/home/{self.user}/.nohan/data/libraries_list', 'r')
            elements = [element.split() for element in my_file]
        else:
            os.system(f'touch /home/{self.user}/.nohan/data/libraries_list')
            my_file = open(f'/home/{self.user}/.nohan/data/libraries_list', 'r')
        my_file.close()
        count = 0
        if len(elements) > 0:
            self.libraries = []
            print('########### Bibliotecas ###########')
            for element in elements:
                library = str(element)
                library = library[2:len(library)-2]
                print(f'{count}.', str(library))
                self.libraries.append(library)
                count+=1
            print('')
            print('Enter para continuar')
            print('###################################')
            input()
        else:
            print('No hay elementos')
            print('Enter para continuar')
            input()

    def add_library(self):
        # Add the library to list
        print('')
        if self.validate():
            option = input(str('Confirmación: ¿Deseas agregar la nueva '
                'biblioteca a la lista? [s/n] pred(s): '))
            if option != 'n':
                n_library = f'{self.library_path}'
                self.libraries.append(n_library)
                self.library_path = ''
                my_file = open(f'/home/{self.user}/.nohan/data/libraries_list',
                        'w')
                my_file.write('\n'.join(self.libraries))
                my_file.close()
                print('Lista de bibliotecas actualizada.')

    def validate(self):
        # Return true if the user input a library, else return
        # false and print a message
        msj = ''
        self.library_path = input(str('Indica la ruta de la biblioteca: '))

        if len(self.library_path) == 0:
            msj += 'Debes indicar la ruta de la biblioteca.'
        if len(msj) > 0:
            print(f'Error: {msj}')
            return False
        return True

    def delete(self):
        # Delete a library after of confirm
        if len(self.libraries) > 0:
            self.list_libraries()
            library_num = input(str('Ingresa el número de índice de la'
                'biblioteca: '))
            if library_num.isnumeric():
                option = input(str('Confirmación: ¿Eliminar la biblioteca de la '
                'lista? [s/n] pred(n): '))
                self.libraries.remove(self.libraries[int(library_num)])
                my_file = open(f'/home/{self.user}/.nohan/data/libraries_list',
                        'w')
                my_file.write('\n'.join(self.libraries))
                my_file.close()
                print('La lista ha sido actualizada.')

