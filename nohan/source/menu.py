#!/usr/bin/env python3

__author__ = "Jona3717"

import curses, os, sys


class Menu:
    
    def __init__(self):
        # Inicializa una nueva ventana para capturar las pulsaciones de teclas
        self.scr = curses.initscr()
        # Evita que se genere una segunda pulsación automáticamente
        curses.noecho()
        # Deshabilita el almacenamiento en el buffer
        curses.cbreak()
        # Permite usar colores al resaltar la opción seleccionada
        curses.start_color()
        # Captura el ingreso por teclado
        self.scr.keypad(1)
        # Configura los colores del menú en blanco y negro
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        
    def menu_options(self, options=(), title='', subtitle=''):
        """ Esta función, muestra el título, subtítulo y las opciones
        tomándolos desde los argumentos."""
        # Indica la la opción resaltada. Siempre inicia en uno al iniciar el menú.
        pos = 1
        # Controla el loop y navega entre las opciones hasta pulsar enter
        x = None
        # Color para la opción resaltada
        h = curses.color_pair(1)
        # Color para la opción no resaltada
        n = curses.A_NORMAL
        
        while x != ord('\n'):
            # Limpia la pantalla anterior y la actualiza de acuerdo a pos
            self.scr.clear()
            self.scr.border(0)
            # Título del menú
            self.scr.addstr(2,2, title, curses.A_STANDOUT)
            # Subtítulo del menú
            self.scr.addstr(4,2, subtitle, curses.A_BOLD)
            # Contador que indica la línea en la que se mostrarán las opcions
            line = 5
            # Contador que permite comparar la opción resaltada
            count = 1
            
            for i in options:
                """ Recorre y compara las opciones, para mostrarlas en
                pantalla y resaltar la opción seleccionada."""
                if pos == count:
                    # Muestra la opción resaltada
                    self.scr.addstr(line, 4, f'{count}. {i}', h)
                else:
                    # Muestra la opción sin resaltar
                    self.scr.addstr(line, 4, f'{count}. {i}', n)
                count += 1
                line += 1
            self.scr.refresh()
            # Captura el ingreso por teclado
            x = self.scr.getch()
            
            # Indica si ha cumplido la condición
            value = None
            for i in range(len(options)):
                if x == ord(str(i+1)):
                    pos = i + 1
                    value = True
            if not value:
                if x == 258:
                    if pos < len(options):
                        pos += 1
                    else:
                        pos = 1
                elif x == 259:
                    if pos > 1:
                        pos -= 1
                    else:
                        pos = len(options)
                elif x != ord('\n'):
                    curses.flash()
        
        curses.endwin()

        return str(pos)

    
