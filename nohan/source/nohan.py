__author__ = "Jona3717"

import sys
import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify
from source.utilidades import Utilidades


metodo = Utilidades()

def Nohan():

    """Función principal, captura el parámetro ingresado por el usuario
    y lo pasa a la función activar.""" 
    try:
        parametro = sys.argv[1].lower()
        Notify.init("NoHan")
        activar(parametro)
    except IndexError:
        notif("Debes pasar un argumento. Para ver la ayuda puedes escribir: nohan ayuda")

def activar(param):
    """ Toma el parámetro y retorna el valor de acuerdo a
    lo establecido en el diccionario opciones."""
    try:
        opciones.get(param)()
    except:
        notif("Lo siento, no puedo ayudarte con eso")

def notif(text):
    # Muestra una notificación flotante con el texto indicado
    Notify.Notification.new(text).show()

def buscar():
    """Concatena la búsqueda ingresada por el usuario y la envía a
    la función buscar de la clase Utilidades"""
    try:
        if sys.argv[2].lower():
            lista = sys.argv[2:]
            busqueda = ""
            for i in lista:
                busqueda += '+' + i
            metodo.buscar(busqueda)
    except IndexError:
        notif("No hay nada para buscar")

def abrir():
    """Envía el nombre del programa a la función
    ejecutarPrograma de Utilidades, si la función devuelve False, muestra un mensaje"""
    try:
        if sys.argv[2].lower():
            programa = sys.argv[2]
            if metodo.ejecutarPrograma(programa):
                pass
            else:
                notif("No se encontró {} en la lista".format(programa))
    except IndexError:
        notif("Nada para abrir")

def musica():
    """Envía el nombre de la biblioteca a la función reproducirMusica de Utiliidades,
    si la función devuelve False, muestra un mensaje"""
    try:
        if sys.argv[2].lower():
            biblioteca = sys.argv[2]
            if metodo.reproducirMusica(biblioteca):
                pass
            else:
                notif("No se encontró {} en la lista".format(biblioteca))
    except IndexError:
        notif("No ingresaste el nombre de la biblioteca")

opciones = {
        "init" : metodo.nohan_voz,
        "busca" : buscar,
        "info" : metodo.acercaDe,
        "programas" : metodo.agregarProgramas,
        "abre" : abrir,
        "quiero" : abrir,
        "reproduce" : musica,
        "bibliotecas" : metodo.agregarBibliotecas,
        "actualiza" : metodo.actualizar,
        "ayuda" : metodo.ayuda
}

