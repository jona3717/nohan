__author__ = 'Jona3717'

import subprocess
import sys
import os
import os.path as path
from source.menu import Menu

class ProgramsList:
    # This class content the add, list and remove programs methods
    def __init__(self):
        self.program_path = ''
        self.program_alias = ''
        self.programs = []

        self.user = self.user_id()
        self.menu()

    def menu(self):
        options = {
                '1':self.add_program,
                '2':self.delete,
                '3':self.list_programs,
                '0':sys.exit
                }
        list_options = ('Agregar', 'Quitar', 'Listar', 'Cancelar')
        d_menu = Menu()
        option = d_menu.menu_options(list_options, 'Menu Programas',
                                     'Selecciona una opción')
        if len(option) > 0 and option.isnumeric():
            os.system('clear')
            options.get(option, sys.exit)()
            self.menu()

    def user_id(self):
        # Identify the user name and return this
        out = subprocess.check_output('whoami', shell=True).splitlines()
        element = str(out[0])
        user = element[2:len(element)-1]
        return user

    def list_programs(self):
        # Open the file that contains the aliases and the names of programs to
        # add dis in the list
        if path.exists(f'/home/{self.user}/.nohan/data/programs_list'):
            my_file = open(f'/home/{self.user}/.nohan/data/programs_list', 'r')
            elements = [element.split() for element in my_file]
        else:
            os.system(f'touch /home/{self.user}/.nohan/data/programs_list')
            my_file = open(f'/home/{self.user}/.nohan/data/programs_list', 'r')
        my_file.close()
        count = 0
        if len(elements) > 0:
            self.programs = []
            print('########### Programas ###########')
            for element in elements:
                program = str(element)
                program = program[2:len(program)-2]
                print(f'{count} -', str(program))
                self.programs.append(program)
                count+=1
            print('')
            print('Enter para continuar')
            print('#################################')
            input()
        else:
            print('No hay elementos')
            print('Enter para continuar')
            input()

    def add_program(self):
        # Add the alias and the program to list
        print('')
        if self.validate():
            option = input(str('Confirmación: ¿Deseas agregar el nuevo '
                'programa a la lista? [s/n] pred(s): '))
            if option != 'n':
                n_program = f'{self.program_path}-{self.program_alias}'
                self.programs.append(n_program)
                self.program_path = ''
                self.program_alias = ''
                my_file = open(f'/home/{self.user}/.nohan/data/programs_list',
                        'w')
                my_file.write('\n'.join(self.programs))
                my_file.close()
                print('Lista de programas actualizada.')

    def validate(self):
        # Return true if the user input a program and an alias, else return
        # false and print a message
        msj = ''
        self.program_alias = input(str('Indica el alias: '))
        self.program_path = input(str('Indica la ruta del programa: '))

        if len(self.program_alias) == 0:
            msj += 'Debes ingresar un alias.\n'
        if len(self.program_path) == 0:
            msj += 'Debes indicar la ruta del programa.'
        if len(msj) > 0:
            print(f'Error: {msj}')
            return False
        return True

    def delete(self):
        # Delete a program after of confirm
        if len(self.programs) > 0:
            self.list_programs()
            program_num = input(str('Ingresa el número de índice del programa: '))
            if program_num.isnumeric():
                option = input(str('Confirmación: ¿Eliminar el programa de la '
                'lista? [s/n] pred(n): '))
                self.programs.remove(self.programs[int(program_num)])
                my_file = open(f'/home/{self.user}/.nohan/data/programs_list',
                        'w')
                my_file.write('\n'.join(self.programs))
                my_file.close()
                print('La lista ha sido actualizada.')

