#!/usr/bin/env python3

__author__ = 'Jona3717'

import os
import sys
import threading
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from voz import Voz

value = bool

def threads_init(function):
    # Init voice recognition
    try:
        thread_name = threading.Thread(target=function)
        return thread_name

    except:
        print('Unable to start thread')

def start_vr():
    # Instance the Voz class
    voice = Voz()
    global value
    value = True
    while value:
        voice.start()

thread_name = threads_init(start_vr)

def stop_vr():
    global value
    value = False
    thread_name.join()
    sys.exit()

def tray_init():
    # System tray application
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)

    # Create the icon
    icon = QIcon('/usr/share/nohan/source/resources/images/Nohan_Icon_x32.png')

    # Create the tray
    tray = QSystemTrayIcon()
    tray.setIcon(icon)
    tray.setVisible(True)

    # Create menu
    menu = QMenu()
    action = QAction("Iniciar reconocimiento")
    action.triggered.connect(thread_name.start)
    menu.addAction(action)

    # Add stop option to the menu
    stop = QAction('Detener y salir')
    stop.triggered.connect(stop_vr)
    menu.addAction(stop)

    # Add the menu to the tray
    tray.setContextMenu(menu)

    sys.exit(app.exec())

tray_init()
