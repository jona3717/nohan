__author__ = "Jona3717"

import os
import os.path as path
import gi
import subprocess

gi.require_version('Notify', '0.7')
from gi.repository import Notify
from gtts import gTTS as voz
try:
    from source.programs_list import ProgramsList
    from source.library_list import LibraryList
except ModuleNotFoundError:
    from programs_list import ProgramsList
    from library_list import LibraryList

class Utilidades:
    # La clase contiene los métodos utilizados por la aplicación para su funcionamiento.

    def __init__(self):
        pass

    def nombre_usuario(self):
        # Captura el nombre de usuario con la ayuda del comando whoami
        salida = subprocess.check_output('whoami', shell=True).splitlines()
        cadena = str(salida[0])
        usuario = cadena[2:len(cadena) - 1]
        return usuario

    def terminal(self):
        # Extrae el nombre de la terminal
        usuario = self.nombre_usuario()
        f = open(f'/home/{usuario}/.nohan/data/term-emulator', 'r')
        terminal = f.read()
        f.close()
        return terminal

    def habla(self, texto, nombre):
        # Permite reproducir notificaciones de voz.
        nombre = nombre + ".mp3"
        usuario = self.nombre_usuario()
        if path.exists("/home/%s/.nohan/audio/%s" % (usuario, nombre)):
            os.system("mpg123 /home/%s/.nohan/audio/%s" % (usuario, nombre))
        else:
            lenguaje = "es"
            objeto = voz(texto, lenguaje, slow=False)
            objeto.save("/home/%s/.nohan/audio/%s" % (usuario, nombre))
            os.system("mpg123 /home/%s/.nohan/audio/%s" % (usuario, nombre))

    def acercaDe(self):
        # Ejecuta el archivo que contiene la clase "about", que presenta la información sobre nohan y el autor
        term = self.terminal()
        os.system(f'{term} -e /usr/share/nohan/source/about.sh')

    def agregarProgramas(self):
        # Ejecuta el archivo que contiene la clase "ProgramsList", que permite editar la lista de programas que puede
        # ejecutar NoHAN.
        notificacion = 'Abriendo lista de programas'
        Notify.Notification.new(notificacion).show()
        ProgramsList()

    def agregarBibliotecas(self):
        # Ejecuta el archivo que contiene la clase "lista_bibliotecas", que permite editar a lista de biblotecas que
        # puede reproducir NoHaN, haciendo uso de VLC
        notificacion = 'Abriendo lista de bibliotecas'
        Notify.Notification.new(notificacion).show()
        LibraryList()

    def buscar(self, busq):
        # Recibe una cadena de texto y realiza una búsqueda de esta con DuckDuckGo
        if busq:
            notificacion = "Buscando en Duck Duck Go"
            Notify.Notification.new(notificacion).show()
            self.habla(notificacion, "buscando")
            os.system(f'firefox https://duckduckgo.com/?q={busq}&_')

    def reproducirMusica(self, nombre_biblioteca):
        # Recibe una cadena de texto y reproduce la biblioteca indicada, siempre que el alias esté en la lista
        bibliotecas = []
        biblioteca = ''
        usuario = self.nombre_usuario()
        nombre = ''
        pos = int
        valor = True

        archivo = open(f'/home/{usuario}/.nohan/data/libraries_list', 'r')
        elementos = [elemento.split() for elemento in archivo]

        for elemento in elementos:
            biblioteca = str(elemento)
            biblioteca = biblioteca[2:len(biblioteca) - 2]
            bibliotecas.append(biblioteca)
        for i in bibliotecas:
            pos = i.find(nombre_biblioteca)
            if pos > -1:
                nombre = i
                break

        if len(nombre) != 0:
            Notify.Notification.new(f'Reproduciendo {nombre_biblioteca} en VLC').show()
            os.system(f'vlc {nombre}')
            valor = True
        else:
            valor = False
        return valor

    def ejecutarPrograma(self, alias):
        # Recibe una cadena de texto y ejecuta el programa indicado en esta, siempre que el alias esté en la lista
        programas = []
        salida = subprocess.check_output("whoami", shell=True).splitlines()
        cadena = str(salida[0])
        usuario = cadena[2:len(cadena) - 1]
        nombre = ''
        pos = int
        valor = True

        archivo = open(f"/home/{usuario}/.nohan/data/programs_list", 'r')
        elementos = [elemento.split() for elemento in archivo]

        for elemento in elementos:
            programa = str(elemento)
            programa = programa[2:len(programa) - 2]
            programas.append(programa)

        for i in programas:
            pos = i.find(alias)
            if pos > -1:
                nombre = i
                break

        if len(nombre) != 0:
            programa = nombre[:pos - 1]
            Notify.Notification.new('Iniciando %s' % programa).show()
            os.system(programa)
            valor = True
        else:
            valor = False
        return valor

    def actualizar(self):
        # Se determina el tipo de distribución y se lanza el comando para actualizar correspondiente
        notificacion = 'Iniciando actualización del sistema'
        Notify.Notification.new(notificacion).show()
        if path.exists('/etc/arch-release'):
            term = self.terminal()
            os.system(f'{term} -e sudo pacman -Syyu')
        elif path.exists('/etc/debian_version'):
            term = self.terminal()
            os.system(f'{term} -e sudo apt update -y && {term} -e sudo apt '
                    'dist-upgrade')
        else:
            Notify.Notification.new('La actualización no está disponible para tu distribución.').show()

    def ayuda(self):
        # Muestra una guía con los comandos que pueden usarse con NoHAN.
        notificacion = 'Mostrando guía de usuario'
        Notify.Notification.new(notificacion).show()
        term = self.terminal()
        os.system(f'{term} -e /usr/share/nohan/source/resources/ayuda.sh')

    def nohan_voz(self):
        os.system('cd /usr/share/nohan/source && python3 /usr/share/nohan/source/systray.py')
