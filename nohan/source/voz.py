__author__ = "Jona3717"

import subprocess
import speech_recognition as sr
import gi
gi.require_version('Notify', '0.7')
from gi.repository import Notify
from utilidades import Utilidades



class Voz:

    def __init__(self):
        # Se crean el objeto commands y la instancia metodo
        self.commands = []
        self.metodo = Utilidades()

    def start(self):
        #  se cargan los comandos de reconocimiento de voz del usuario
        self.load_commands()
        print("Di algo")
        # se crean las instancias recognizer y microphone
        recognizer = sr.Recognizer()
        microphone = sr.Microphone()
        # se crean las instancias speeched y speech
        speeched = self.capture_transcrib_speech(recognizer, microphone)
        speech = self.voice_recognized(speeched)
        # ejecuta las funciones de acuerdo a los comandos de voz
        self.options(speech)

    def load_commands(self):
        # Carga los comandos grabados por el usuario durante la instalación
        # para el reconocimiento de voz
        # Identifica el nombre del usuario
        output = subprocess.check_output("whoami", shell=True).splitlines()
        phrase = str(output[0])
        user = phrase[2:len(phrase) - 1]
        # Hacemos un try para buscar el archivo de comandos de voz del usuario
        try:
            file = open('/home/{}/.nohan/data/comandos-usuario-voz'.format(user), 'r')
            string_commands = file.read()
            # Se actualiza el objeto commands
            self.commands = string_commands.splitlines()
        except FileNotFoundError:
            # Notifica al usuario que no se encontró el archivo con los comandos de voz
            self.metodo.habla("Los comandos de voz del usuario no han sido encontrados. "
                              "Por favor vuelve a instalar nojan y configura el reconocimiento de voz.",
                              "error-comandos-usuario")


    def capture_transcrib_speech(self, recognizer, microphone):
        """Transcribe lo que se graba desde `microphone`.

        Devuelve un diccionario con tres claves:
        "success":  un booleano indicando si la solicitud de la API
                    fue exitosa.
        "error":   `None` si no ocurre ingún error, de otro modo una cadena que contenga
                    un mensaje de error si no se pudo conectar con la API o no
                    se reconoció lo dicho.
        "transcription": `None` si no se transcribió la voz,
                    de otro modo una cadena que contenga el texto transrito
        """
        # verifica que los argumentos recognizer y microphone sean del tipo correcto
        if not isinstance(recognizer, sr.Recognizer):
            raise TypeError("`recognizer` must be `Recognizer` instance")

        if not isinstance(microphone, sr.Microphone):
            raise TypeError("`microphone` must be `Microphone` instance")

        # ajusta la sensibilidad de reconocimiento al ruido de ambiente y graba
        # el audio del micrófono
        with microphone as source:
            recognizer.adjust_for_ambient_noise(source)
            audio = recognizer.listen(source)

        # creamoos ej objeto response
        response = {
            "success": True,
            "error": None,
            "transcription": None
        }

        # hacemos un try del reconocimiento de voz en la grabación
        # si hay una exepción RequestError o UnknownValueError, se
        # captura para actualizar el objeto response respectivamente
        try:
            response["transcription"] = recognizer.recognize_google(audio, language="es-ES").lower()
        except sr.RequestError:
            # no se puede conectar a la API o no responde
            response["success"] = False
            response["error"] = "API unavailable"
        except sr.UnknownValueError:
            # no se entiende lo que se dijo
            response["error"] = "Unable to recognize speech"

        return response

    def voice_recognized(self, speeched):
        """Devuelve lo transcrito seccionado en dos
            partes dentro de un objeto parameters:
            call: contiene el comando que llama a nohan.
            arguments: los argumentos que le pasamos para que
            realice algo
        """
        # Creamos el objeto parameters
        parameters = {
            "call": None,
            "arguments": None
        }
        # Evalua si se ha capturado audio y divide lo capturdo
        # si hay algún error, lo muestra por consola.
        if speeched["transcription"]:
            print("Dijiste: {}".format(speeched["transcription"]))
            voz = "{}".format(speeched["transcription"])
            words = voz.split()
            # Actualiza el objeto parameters
            parameters["call"] = " ".join(words[:2])
            print(parameters["call"])
            try:
                parameters["arguments"] = words[2:]
            except IndexError:
                notification = 'Debes pasar un argumento. Para ver la ayuda puedes escribir: nohan ayuda'
                print(notification)
        if not speeched["success"]:
            print("ERROR: No ha podo conectarse con la API")
        if speeched["error"]:
            print("ERROR: {}".format(speeched["error"]))

        return parameters

    def options(self, speech):
        """Analiza que el comando sea el correcto, de ser así
            verifica que el parámetro coincida y realiza la
            acción de acuerdo al mismo
        """
        busqueda = ""
        Notify.init('NoHan')

        if speech["call"]:
            if speech["call"] in self.commands:
                arguments = speech["arguments"]
                argument = arguments[0]
                if argument == "busca" or "que" and "es" in arguments:
                    try:
                        busqueda = "+".join(arguments[1:])
                        self.metodo.buscar(busqueda)
                    except IndexError:
                        notification = "Disculpa, no entendí. ¿qué quieres que busque?"
                        self.metodo.habla(notification, "busqueda-error")
                elif "quien" and "eres" in arguments:
                    self.metodo.acercaDe()
                elif argument == "actualiza" or "actualizar" in arguments:
                    notification = 'Iniciando actualización del sistema'
                    self.metodo.habla(notification, "acualizacion")
                    self.metodo.actualizar()
                elif "agrega" and "programa" in arguments:
                    notification = 'Abriendo lista de programas'
                    self.metodo.habla(notification, "lista-programas")
                    self.metodo.agregarProgramas()
                elif argument == "abre" or argument == "quiero":
                    try:
                        programa = arguments[1]
                        if self.metodo.ejecutarPrograma(programa):
                            pass
                        else:
                            notification = "No pude encontrar {} en la lista de programas.".format(programa)
                            self.metodo.habla(notification, "error-programa")
                    except IndexError:
                        notification = "Disculpa, no escuché el alias del programa"
                        self.metodo.habla(notification, "error-abrir")
                elif "muestra" and "bibliotecas" in arguments:
                    notification = 'Abriendo lista de bibliotecas'
                    self.metodo.habla(notification, "abriendo-lista-bibliotecas")
                    self.metodo.agregarBibliotecas()
                elif argument == "ayuda":
                    notification = 'Mostrando guía de usuario'
                    self.metodo.habla(notification, "mostrando-ayuda")
                    self.metodo.ayuda()
                elif argument == "reproduce":
                    try:
                        biblioteca = arguments[1]
                        if self.metodo.reproducirMusica(biblioteca):
                            pass
                        else:
                            notification = "No se pudo reproducir {}".format(biblioteca)
                            self.metodo.habla(notification, "error-biblioteca")
                    except IndexError:
                        notification = "Dusculpa, no escuché el nombre de la biblioteca"
                        self.metodo.habla(notification, "error-reproducir")
                else:
                    notification = 'Lo siento, no puedo ayudarte con eso'
                    self.metodo.habla(notification, "error-parametro")
        else:
            print("None in speech")

